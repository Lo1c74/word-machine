#pragma once

#include "Export.hpp"

#include <map>
#include <span>
#include <string>
#include <vector>

namespace WGen {

class FollowersMap {
public:
    /*! \brief Gets the number of occurences where a word starts with the given letter.
     * 
     * \param letter The letter starting the word.
     * 
     * \return Returns the number of occurences.
     */
    WGEN_API size_t beginWith(char letter) const noexcept;

    /*! \brief Gets the number of occurences where a given letter is following by another.
     * 
     * \param followed The letter followed by the second one.
     * \param follower The letter following the first one.
     * 
     * \return Returns the number of occurences.
     */
    WGEN_API size_t followedBy(char followed, char follower) const noexcept;

    /*! \brief Gets the number of occurences where a word finishes with the given letter.
     *
     * \param letter The letter finishing the word.
     *
     * \return Returns the number of occurences.
     */
    WGEN_API size_t endWith(char letter) const noexcept;

    /*! \brief Gets the letters starting a word.
     * 
     * \return Returns the letters and their corresponding occurences.
     */
    WGEN_API const std::map<char, size_t>& lettersBeginning() const noexcept;

    /*! \brief Gets the letters following a given letter in a word.
     *
     * \param letter The letter followed.
     * 
     * \return Returns the letters and their corresponding occurences.
     */
    WGEN_API const std::map<char, size_t>& lettersFollowing(char letter) const noexcept;

    /*! \brief Gets the letters finishing a word.
     *
     * \return Returns the letters and their corresponding occurences.
     */
    WGEN_API const std::map<char, size_t>& lettersEnding() const noexcept;

    /*! \brief Checks if the object is empty (meaning that no words have been added).
     * 
     * \return Returns true if it is, false otherwise.
     */
    WGEN_API bool isEmpty() const noexcept;

    /*! \brief Adds the given words to the object.
     * 
     * \param words The words to add.
     */
    WGEN_API void addWords(std::span<const std::string> words);

private:
    std::map<char, size_t> m_beginners {};
    std::map<char, std::map<char, size_t>> m_followers {};
    std::map<char, size_t> m_enders {};
};

/*!
 * \brief Generates a list of words given the letter followers adjacency list.
 * 
 * \param followers The letter followers adjacency list to use.
 * \param nbWordsToGenerate The optional number of words to generate. By default, it is set to 1.
 * 
 * \return Returns a list of words.
 */
WGEN_API std::vector<std::string> generateWords(
    const FollowersMap& followers, size_t nbWordsToGenerate = 1);

}
