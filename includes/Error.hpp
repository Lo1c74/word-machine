#pragma once

#include "Export.hpp"

#include <exception>
#include <filesystem>

namespace WGen {

class FileError : public std::exception {
public:
    enum class Code { NotFound, InvalidExtension };

    WGEN_API FileError(Code code, const std::filesystem::path& path);

    WGEN_API const char* what() const noexcept override;

    WGEN_API Code code() const noexcept;
    WGEN_API const std::filesystem::path& path() const noexcept;

private:
    Code m_code {};
    std::filesystem::path m_path {};
    std::string m_what {};
};

}
