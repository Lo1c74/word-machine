#pragma once

#include "Export.hpp"

#include <exception>
#include <filesystem>
#include <istream>
#include <string>
#include <vector>

namespace WGen {

/*!
 * \brief Parses the given file containing the list of words.
 * The words must be separated by a blank character.
 *
 * \param dictionaryPath The path to the text file containing the list of words.
 *
 * \return Returns a list of words.
 *
 * \throw FileError if the file cannot be opened.
 */
WGEN_API std::vector<std::string> parse(const std::filesystem::path& dictionaryPath);

/*!
 * \brief Parses the given stream containing the list of words.
 * The words must be separated by a blank character.
 *
 * \param stream The stream containing the list of words.
 *
 * \return Returns a list of words.
 */
WGEN_API std::vector<std::string> parse(std::istream& stream);

}
