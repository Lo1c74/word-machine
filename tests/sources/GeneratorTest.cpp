#include <Generator.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <string>
#include <vector>

using namespace std::string_literals;

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// makeFollowersList
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

TEST(Generator,
    Calling_makeFollowersLetterList_With_An_Empty_List_Of_Words_Should_Return_An_Empty_List)
{
    const auto followers = []() {
        WGen::FollowersMap map;
        map.addWords({});
        return map;
    }();

    EXPECT_TRUE(followers.isEmpty());
}

TEST(Generator,
    Calling_makeFollowersLetterList_With_A_List_Of_Words_Containing_A_Empty_Word_Should_Return_An_Empty_List)
{
    const auto followers = []() {
        WGen::FollowersMap map;
        map.addWords(std::vector { ""s });
        return map;
    }();

    EXPECT_TRUE(followers.isEmpty());
}

TEST(Generator,
    Calling_makeFollowersLetterList_With_A_List_Of_One_Word_Should_Return_The_Appropriate_List)
{
    const auto followers = []() {
        WGen::FollowersMap map;
        map.addWords(std::vector { "abc"s });
        return map;
    }();

    EXPECT_EQ(followers.beginWith('a'), 1);
    EXPECT_EQ(followers.followedBy('a', 'b'), 1);
    EXPECT_EQ(followers.followedBy('b', 'c'), 1);
    EXPECT_EQ(followers.endWith('c'), 1);
}

TEST(Generator,
    Calling_makeFollowersLetterList_With_A_List_Of_Words_Should_Return_The_Appropriate_List)
{
    const auto followers = []() {
        WGen::FollowersMap map;
        map.addWords(std::vector { "abc"s, "abd"s });
        return map;
    }();

    EXPECT_EQ(followers.beginWith('a'), 2);
    EXPECT_EQ(followers.followedBy('a', 'b'), 2);
    EXPECT_EQ(followers.followedBy('b', 'c'), 1);
    EXPECT_EQ(followers.followedBy('b', 'd'), 1);
    EXPECT_EQ(followers.endWith('c'), 1);
    EXPECT_EQ(followers.endWith('d'), 1);
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// generateWords
////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////

TEST(Generator, Calling_generateWords_With_An_Empty_Adjacency_List_Should_Return_Empty_Words)
{
    const auto words { WGen::generateWords({}) };

    EXPECT_THAT(words, testing::Each(testing::IsEmpty()));
}

TEST(Generator,
    Calling_generateWords_With_A_Number_Of_Words_To_Generate_Equal_To_Zero_Should_Return_An_Empty_List)
{
    const auto followers = [] {
        WGen::FollowersMap map;
        map.addWords(std::vector { "aa"s });
        return map;
    }();

    EXPECT_THAT(WGen::generateWords(followers, 0), testing::IsEmpty());
}

TEST(Generator,
    Calling_generateWords_With_A_Number_Of_Words_To_Generate_Should_Return_A_List_Containing_This_Number_Of_Words)
{
    const auto followers = [] {
        WGen::FollowersMap map;
        map.addWords(std::vector { "aa"s });
        return map;
    }();

    EXPECT_THAT(WGen::generateWords(followers, 42), testing::SizeIs(42));
}
