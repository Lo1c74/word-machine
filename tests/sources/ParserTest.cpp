#include <Error.hpp>
#include <Parser.hpp>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <filesystem>
#include <fstream>
#include <sstream>
#include <string_view>

class ParserTest : public testing::Test {
protected:
    ParserTest()
    {
        std::filesystem::remove_all(s_directory);
        std::filesystem::create_directory(s_directory);
    }

    void createFile(std::string_view filename) { std::ofstream file { s_directory / filename }; }

    static inline const auto s_directory { std::filesystem::temp_directory_path() / "wgen_tests" };
};

TEST_F(ParserTest, Calling_parse_With_A_Non_Existing_Path_Should_Throw_FileError)
{
    const auto filePath { s_directory / "not_exiting_file.txt" };

    EXPECT_THROW(
        [&] {
            try {
                WGen::parse(filePath);
            } catch (const WGen::FileError& e) {
                EXPECT_EQ(e.code(), WGen::FileError::Code::NotFound);
                EXPECT_EQ(e.path(), filePath);
                throw;
            }
        }(),
        WGen::FileError);
}

TEST_F(ParserTest, Calling_parse_With_An_Invalid_File_Extension_Should_Throw_FileError)
{
    const auto filePath { s_directory / "file.nop" };
    createFile(filePath.filename().string());

    EXPECT_THROW(
        [&] {
            try {
                WGen::parse(filePath);
            } catch (const WGen::FileError& e) {
                EXPECT_EQ(e.code(), WGen::FileError::Code::InvalidExtension);
                EXPECT_EQ(e.path(), filePath);
                throw;
            }
        }(),
        WGen::FileError);
}

TEST(Parser, Calling_parse_With_A_Empty_Stream_Should_Return_An_Empty_List)
{
    std::istringstream content { "" };

    const auto words { WGen::parse(content) };

    EXPECT_THAT(words, testing::IsEmpty());
}

TEST(
    Parser, Calling_parse_With_A_Stream_Containing_A_Word_Should_Return_A_List_Containing_This_Word)
{
    std::istringstream content { "test" };

    const auto words { WGen::parse(content) };

    EXPECT_THAT(words, testing::ElementsAre("test"));
}

TEST(Parser,
    Calling_parse_With_A_Stream_Containing_Many_Words_On_The_Same_Line_Should_Return_A_List_Containing_This_Words)
{
    std::istringstream content { "test1 test2" };

    const auto words { WGen::parse(content) };

    EXPECT_THAT(words, testing::ElementsAre("test1", "test2"));
}

TEST(Parser,
    Calling_parse_With_A_Stream_Containing_Many_Words_On_Many_Lines_Should_Return_A_List_Containing_This_Words)
{
    std::istringstream content { "test1\ntest2" };

    const auto words { WGen::parse(content) };

    EXPECT_THAT(words, testing::ElementsAre("test1", "test2"));
}

TEST(Parser, Calling_parse_With_A_Stream_An_Empty_Line_Should_Not_Keep_This_Line)
{
    std::istringstream content { "test1\n\ntest2" };

    const auto words { WGen::parse(content) };

    EXPECT_THAT(words, testing::ElementsAre("test1", "test2"));
}

TEST(Parser, Calling_parse_With_Blank_Characters_Should_Not_Keep_Them)
{
    std::istringstream content { " test1\ttest2\r\n" };

    const auto words { WGen::parse(content) };

    EXPECT_THAT(words, testing::ElementsAre("test1", "test2"));
}
