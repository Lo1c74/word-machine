#include "Generator.hpp"

#include <algorithm>
#include <numeric>
#include <random>
#include <ranges>

namespace WGen {

size_t FollowersMap::beginWith(char letter) const noexcept { return m_beginners.at(letter); }

size_t FollowersMap::followedBy(char followed, char follower) const noexcept
{
    return m_followers.at(followed).at(follower);
}

size_t FollowersMap::endWith(char letter) const noexcept { return m_enders.at(letter); }

const std::map<char, size_t>& FollowersMap::lettersBeginning() const noexcept
{
    return m_beginners;
}

const std::map<char, size_t>& FollowersMap::lettersFollowing(char letter) const noexcept
{
    return m_followers.at(letter);
}

const std::map<char, size_t>& FollowersMap::lettersEnding() const noexcept { return m_enders; }

bool FollowersMap::isEmpty() const noexcept { return m_beginners.empty(); }

void FollowersMap::addWords(std::span<const std::string> words)
{
    auto nonEmptyWords { words
        | std::views::filter([](std::string_view word) { return !word.empty(); }) };

    for (std::string_view word : nonEmptyWords) {
        m_beginners[word[0]]++;
        for (size_t i = 0; i < word.length() - 1; ++i) {
            m_followers[word[i]][word[i + 1]]++;
        }
        m_enders[word[word.length() - 1]]++;
    }
}

std::vector<std::string> generateWords(const FollowersMap& followers, size_t nbWordsToGenerate)
{
    if (followers.isEmpty()) {
        return std::vector<std::string>(nbWordsToGenerate);
    }

    const auto mapPartialSum = [](const std::map<char, size_t>& map) {
        std::map<char, size_t> summedMap(map.begin(), std::next(map.begin()));
        for (const auto& [letter, nbOccurences] : map | std::views::drop(1) | std::views::common) {
            summedMap[letter] = summedMap.end()->second + nbOccurences;
        }
        return summedMap;
    };

    const auto generateLetter = [&followers, &mapPartialSum](char fromLetter) {
        const auto& letters { fromLetter ? followers.lettersFollowing(fromLetter)
                                         : followers.lettersBeginning() };
        const auto summedLetters { mapPartialSum(letters) };
        const auto totalOccurences { std::prev(summedLetters.end())->second
            + (fromLetter ? followers.endWith(fromLetter) : 0) };

        std::random_device randomDevice;
        std::mt19937_64 generator { randomDevice() };
        std::uniform_int_distribution<size_t> distribution { 0, totalOccurences };
        const auto draw { distribution(generator) };

        const auto it { std::ranges::find_if(
            summedLetters, [&draw](const auto& elem) { return elem.second >= draw; }) };
        return it == summedLetters.end() && fromLetter ? static_cast<char>(0) : it->first;
    };

    std::vector<std::string> words(nbWordsToGenerate);
    std::ranges::generate(words, [&] {
        std::string word {};
        char generatedLetter { 0 };
        do {
            generatedLetter = generateLetter(generatedLetter);
            if (generatedLetter) {
                word.push_back(generatedLetter);
            }
        } while (generatedLetter);
        return word;
    });

    return words;
}

}
