#include "Parser.hpp"
#include "Error.hpp"

#include <algorithm>
#include <fstream>

namespace WGen {

std::vector<std::string> parse(const std::filesystem::path& dictionaryPath)
{
    if (!std::filesystem::exists(dictionaryPath)) {
        throw FileError { FileError::Code::NotFound, dictionaryPath };
    }

    if (dictionaryPath.extension().string() != "txt") {
        throw FileError { FileError::Code::InvalidExtension, dictionaryPath };
    }

    std::ifstream stream { dictionaryPath };

    return parse(stream);
}

std::vector<std::string> parse(std::istream& stream)
{
    std::vector<std::string> words;

    std::string tmp;
    while (stream >> tmp) {
        words.push_back(std::move(tmp));
    }

    return words;
}

}
