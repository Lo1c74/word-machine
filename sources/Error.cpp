#include "Error.hpp"

#include <string>
#include <format>

using namespace std::string_literals;

namespace WGen {

FileError::FileError(Code code, const std::filesystem::path& path)
    : std::exception {}
    , m_code { code }
    , m_path { path }
{
    switch (m_code) {
    case Code::NotFound:
        m_what = std::format("the file {} does not exist", m_path.string());
        break;
    case Code::InvalidExtension:
        m_what = std::format("the file extension of {} is invalid", m_path.string());
        break;
    default:
        m_what.clear();
        break;
    }
}

const char* FileError::what() const noexcept { return m_what.c_str(); }

FileError::Code FileError::code() const noexcept { return m_code; }

const std::filesystem::path& FileError::path() const noexcept { return m_path; }

}
